const SockJS = require('sockjs-client');
var sock = new SockJS('https://www.bit4you.io/socketapi');
sock.onopen = function () {
  console.log('Connection etablish');
  sock.send(JSON.stringify({
    api: 'subscribe',
    data: {
      summaries: 'USDT-*'
    }
  }));
};

sock.onmessage = function (e) {
  console.log('message', JSON.parse(e.data));
};

sock.onclose = function () {
  console.log('close');
};
