const WebSocket = require('ws');
var sock = new WebSocket('https://www.bit4you.io/socketapi/websocket');
sock.onopen = function () {
  console.log('Connection etablish');
  sock.send(JSON.stringify({
    api: 'subscribe',
    data: {
      summaries: 'USDT-*'
    }
  }));
};

sock.onmessage = function (e) {
  console.log('message', JSON.parse(e.data));
};

sock.onclose = function () {
  console.log('close');
};
