#!/usr/bin/env node

const EventEmitter = require('events');
if (typeof (WebSocket) === "undefined")
    global.WebSocket = require("ws");

class bit4youWS extends EventEmitter {
    constructor() {
        super();
        this.connect();
    }

    get ready() {
        return this.client && Number(this.client.readyState) === 1;
    }

    connect() {
        //Connect to bit4you websocket
        //this.client = new WebSocket(`ws://0.0.0.0:8080/socketapi/websocket`);
        this.client = new WebSocket(`wss://www.bit4you.io/socketapi/websocket`);

        //Listen to connection open event
        this.client.on('open', (data) => {
            console.log('Socket connected!');
        });

        //Listen to connection close event
        this.client.on('close', () => {
            console.warn('Socket disconnected!');
        });

        //Listen messages comming from websocket
        this.client.on('message', (data) => {
            if(!data)
                return;

            //Parsing received JSON data
            try {
                data = JSON.parse(data);
            } catch(e) {
                return console.error('Error parse received data', e);
            }

            //Handle received data
            if(data.apiError)
                return this.handleApiError(data);
            else if(data.api)
                return this.handleApi(data);
            else
                console.log(data)
        });
    }

    handleApiError(data) {
        //If a salt has been bound to the data, emit with salt, else just emit witth the api name
        if(data.salt)
            this.emit(data.salt, data.error);
        else
            this.emit(data.apiError, data.error);
    }

    handleApi(data) {
        //If a salt has been bound to the data, emit with salt, else just emit witth the api name
        if(data.salt)
            this.emit(data.salt, data.data);
        else if(data.api)
            this.emit(data.api, data.data);
        else
            console.log(data)
    }

    send(data) {
        return new Promise((resolve, reject) => {
            if (!this.ready) {
                //If socket is not ready yet, wait until a socket is ready and then send the requested data
                var interval = setInterval(() => {
                    if(this.ready) {
                        clearInterval(interval);
                        resolve(this.send(data));
                    }
                }, 100);

            } else {

                //Create a new request id, used as a salt to identity the request.
                this.request_id = (this.request_id || 0) + 1;
                const payload = Object.assign({ salt: this.request_id }, data);

                //Stringify our payload and send it to the websocket
                this.client.send(JSON.stringify(payload));

                //Wait until the salt has been emited, and return the received data
                this.once(payload.salt, (res) => {
                    if(res.error)
                        reject(res);
                    else
                        resolve(res);
                });

            }
        });
    }

    api(api, data) {
        return this.send({
            api: api,
            data: (data || {})
        })
    }

    ping() {
        return this.api('ping')
    }

    subscriptions() {
        return this.api('subscriptions');
    }

    unsubscribeAll() {
        return this.api('unsubscribe', {
            all: true
        });
    }

    unsubscribe(config) {
        return this.api('unsubscribe', config);
    }

    subscribe(config, removeSubscriptions = false) {
        return this.api('subscribe', Object.assign(config, {
            removeSubscriptions
        }));
    }
}

var main = async function () {
    const client = new bit4youWS();

    /*const orderbook = await client.api('market/orderbook', {
        market: 'USDT-BTC',
        state: true
    })

    console.log(orderbook)*/

    var subscriptions = await client.subscribe({
        //summaries: '*',
        //summaries: 'USDT-BTC',
        //summaries: 'USDT-*,BTC-ETH'
        //summaries: [ 'USDT-ETH', '*-LTC', 'USDT-TRX' ],

        history: 'USDT-BTC,USDT-ETH',
        //history: '*',
        //history: 'USDT-*,BTC-ETH'
        //history: [ 'USDT-ETH', 'BTC-ETH', 'USDT-TRX' ],

        //orderbook: 'USDT-BTC', //Updated on 25 levels depth
        //orderbook: 'USDT-BTC',
        //orderbook: 'USDT-*,BTC-ETH'
        //orderbook: [ 'USDT-ETH', 'BTC-ETH', 'USDT-TRX' ],
    });

    /*subscriptions = await client.unsubscribe({
        summaries: 'BTC-*,ETH-*',
        //history: 'BTC-*,ETH-*',
        //orderbook: 'BTC-*,ETH-*',
    });*/

    console.log(subscriptions)

    client.on('market/summaries', (summaries) => {
        console.log('got summaries', summaries)
    })

    client.on('market/history/live', (histories) => {
        for(var history of histories) {
            console.log('got history items', history)
        }
    })

    client.on('market/orderbook/live', (histories) => {
        console.log('got orderbook items', histories)
    })

    //const marketList = await client.api('market/list');
    //console.log(marketList);

};

main().then(console.log).catch(console.error);
