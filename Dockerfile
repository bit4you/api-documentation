# europe-west1-docker.pkg.dev/bit4you-cloud/bit4you/api-docs
# signature: J/cF0K+52HmLsGDaxvkYobmQea3XDy0qpbsXxqnNesE=
FROM node:alpine as builder

COPY package.json /var/www/package.json
COPY src /var/www/src

WORKDIR /var/www
RUN npm i && \
    npm i mobx@^4.0.0 styled-components

RUN npm run bundle

FROM nginx:alpine

COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /var/www/build.html /usr/share/nginx/html/index.html

EXPOSE 80 443

CMD ["nginx"]